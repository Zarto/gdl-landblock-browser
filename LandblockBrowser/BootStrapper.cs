﻿using Caliburn.Micro;
using LandblockBrowser.ViewModels;
using LBDataManagerLibrary.Loaders;
using LBDataManagerLibrary.Processors;
using Lifestoned.DataModel.Gdle.Spawns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace LandblockBrowser
{
    public class BootStrapper: BootstrapperBase
    {
        private SimpleContainer _container = new SimpleContainer();

        public BootStrapper()
        {
            Initialize();
        }

        protected override void Configure()
        {
            _container.Instance(_container);

            _container.PerRequest<ISpawnsLoader, SpawnsLoader>()
                .PerRequest<ISearchForEntity, SearchForEntity>()
                .PerRequest<SpawnMap>();

            _container
                .Singleton<IWindowManager, WindowManager>()
                .Singleton<IEventAggregator, EventAggregator>();

            GetType().Assembly.GetTypes()
                .Where(type => type.IsClass)
                .Where(type => type.Name.EndsWith("ViewModel"))
                .ToList()
                .ForEach(viewModelType => _container.RegisterPerRequest(
                    viewModelType, viewModelType.ToString(), viewModelType));
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<MainWindowViewModel>();
        }
        ////OLD INJECTION METHOD
        //protected override object GetInstance(Type service, string key)
        //{
        //    return base.GetInstance(service, key);
        //}
        protected override object GetInstance(Type service, string key)
        {
            var instance = _container.GetInstance(service, key);
            if (instance != null)
                return instance;
            throw new InvalidOperationException("Could not locate any instances.");
        }

        protected override IEnumerable<object> GetAllInstances(Type service) 
        {
            return _container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            _container.BuildUp(instance);
        }
    }

}
