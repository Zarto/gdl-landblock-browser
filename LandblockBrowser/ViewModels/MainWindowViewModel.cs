﻿using Caliburn.Micro;
using LBDataManagerLibrary.Loaders;
using LBDataManagerLibrary.Models;
using LBDataManagerLibrary.Processors;
using LBDataManagerLibrary.Utilities;
using Lifestoned.DataModel.Gdle.Spawns;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace LandblockBrowser.ViewModels
{
    public class MainWindowViewModel : Screen, IHandle<LoadReport>
    {
        private IEventAggregator _events;
        public MainWindowViewModel(IEventAggregator events, SpawnMap spawns)
        {
            _events = events;
            _events.SubscribeOnUIThread(this);
            _spawns = spawns;
        }

        private SpawnMap _spawns;
        #region Load SpawnMaps Properties

        public string SpawnMapsDirectoryPath { get; set; }
        private string _completeStatusMessage = "";
        private string _loadSpawnMapsButtonStatusString = "Load All";
        private bool _isLoading;
        private int _progressBarStatus;

        public bool IsLoading
        {
            get { return _isLoading; }
            set
            {
                _isLoading = value;
                NotifyOfPropertyChange(() => IsLoading);
                NotifyOfPropertyChange(() => CanLoadSpawnMapsButton);
            }
        }

        public string LoadSpawnMapsButtonStatusString
        {
            get { return _loadSpawnMapsButtonStatusString; }
            set
            {
                _loadSpawnMapsButtonStatusString = value;
                NotifyOfPropertyChange(() => LoadSpawnMapsButtonStatusString);
            }
        }

        public bool CanLoadSpawnMapsButton
        {
            get
            {
                if (IsLoading || string.IsNullOrWhiteSpace(SpawnMapsDirectoryPath) || !Directory.Exists(SpawnMapsDirectoryPath))
                {
                    return false;
                }

                return true;
            }
        }

        private string _importErrorsDisplay;

        public string ImportErrorsDisplay
        {
            get { return _importErrorsDisplay; }
            set
            {
                _importErrorsDisplay = value;
                NotifyOfPropertyChange(() => ImportErrorsDisplay);
            }
        }

        public int ProgressBarStatus
        {
            get { return _progressBarStatus; }
            set
            {
                _progressBarStatus = value;
                NotifyOfPropertyChange(() => ProgressBarStatus);
            }
        }

        private bool _isProgressBarVisible = false;

        public bool IsProgressBarVisible
        {
            get { return _isProgressBarVisible; }
            set
            {
                _isProgressBarVisible = value;
                NotifyOfPropertyChange(() => IsProgressBarVisible);
            }
        }


        public async void LoadSpawnMapsButton(string spawnMapsDirectoryPath)
        {
            IsLoading = true;
            LoadSpawnMapsButtonStatusString = "Loading...";
            CompleteStatusMessage = "";
            IsProgressBarVisible = true;

            //Do Load Stuff
            var loader = IoC.Get<ISpawnsLoader>();
            _spawns = await Task.Run(() => loader.Load(spawnMapsDirectoryPath));

            //Display Results
            CompleteStatusMessage = $"Loaded {_spawns.Entries.Count} spawnMaps. See Log for errors.";
            ImportErrorsDisplay = loader.CompiledExceptionStrings;

            IsLoading = false;
            IsProgressBarVisible = false;
            LoadSpawnMapsButtonStatusString = "Reload All";
        }
        public string CompleteStatusMessage
        {
            get { return _completeStatusMessage; }
            set
            {
                _completeStatusMessage = value;
                NotifyOfPropertyChange(() => CompleteStatusMessage);
            }
        }

        public Task HandleAsync(LoadReport message, CancellationToken cancellationToken)
        {
            //CompleteStatusMessage = message.LoadingExceptions;
            ProgressBarStatus = message.Progress;
            return Task.CompletedTask;
        }

        #endregion

        #region Search WCID in SpawnMaps Tab Properties
        public string SearchLandblockID { get; set; } = "";
        private Nullable<uint> _searchWCID = null;
        private string _displayBox = "";
        private string _searchName = "";
        public Nullable<uint> SearchWCID
        {
            get { return _searchWCID; }
            set
            {
                _searchWCID = value;
                NotifyOfPropertyChange(() => SearchWCID);
            }
        }

        public string SearchName
        {
            get { return _searchName; }
            set
            {
                _searchName = value;              
                NotifyOfPropertyChange(() => SearchName);
                NotifyOfPropertyChange(() => CanSearchButton);
            }
        }
       
        public string DisplayBox
        {
            get { return _displayBox; }
            set
            {
                _displayBox = value;
                NotifyOfPropertyChange(() => DisplayBox);
            }
        }
        public bool CanSearchButton
        {           
            get
            {
                if (SearchWCID == null && SearchName.Equals(""))
                {
                    return false;
                }

                return true;
            }
        }

        public bool CanClearButton
        {
            get
            {
                if (DisplayBox.Equals(""))
                {
                    return false;
                }

                return true;
            }
        }

        public void SearchButton(Nullable<uint> searchWCID, string searchLandblockID)
        {

            var search = IoC.Get<ISearchForEntity>();

            searchWCID = searchWCID.HasValue ? searchWCID : 0;
            uint searchLandblockDecID = 0;
            if (!SearchLandblockID.Equals(""))
            {
                try
                {
                    searchLandblockDecID = Util.TryConvertToDec(searchLandblockID);
                }
                catch (InvalidOperationException)
                {
                    DisplayBox = "Invalid Landblock ID. Must be a integer value or a hex string value";
                    return;
                }
            }

            var findings = search.Find(_spawns, _searchName, (uint)searchWCID, searchLandblockDecID);

            DisplayBox = findings;
        }

        public void ClearButton(string displayBox) 
        {
            DisplayBox = "";
        }
        #endregion

        #region EditSpawnMap


        #endregion
    }
}


