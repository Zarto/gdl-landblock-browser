﻿using Lifestoned.DataModel.Gdle.Spawns;
using System.Collections.Concurrent;

namespace LBDataManagerLibrary.Loaders
{
    public interface ISpawnsLoader
    {
        ConcurrentBag<string> ExceptionStrings { get; set; }
        string CompiledExceptionStrings { get; set; }

        SpawnMap Load(string directoryPath);
    }
}