﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using Lifestoned.DataModel;
using Lifestoned.DataModel.Gdle.Spawns;
using System.IO;
using System.Threading.Tasks;
using System.Linq;
using Newtonsoft.Json;
using System.Collections.Concurrent;
using Caliburn.Micro;
using LBDataManagerLibrary.Models;
using System.Threading;

namespace LBDataManagerLibrary.Loaders
{
    public class SpawnsLoader : ISpawnsLoader
    {
        private SpawnMap _spawns;
        private IEventAggregator _events;
        public ConcurrentBag<string> ExceptionStrings { get; set; } = new ConcurrentBag<string>();
        public string CompiledExceptionStrings { get; set; } = "";
        private int loadCounter = 0;
        private int loadReportThreshold = 0;

        private string _directoryPath;

        public SpawnsLoader(SpawnMap spawns, IEventAggregator events)
        {
            _spawns = spawns;
            _events = events;
        }

        public SpawnMap Load(string directorypath)
        {
            _directoryPath = directorypath; //assumes valid directory path already determined

            string[] allFiles = GetFiles();
            loadReportThreshold = Math.Max((int)(allFiles.Length * 0.01), 1);
           
            _spawns.Entries = allFiles.AsParallel().Select(fileEntry => ConvertFromJson(ReadFile(fileEntry), fileEntry)).Where(x => x != null).ToList();

            //Build Exceptions Report for file read/convert errors.
            foreach (string exception in ExceptionStrings)
            {
                CompiledExceptionStrings += exception + Environment.NewLine;
            }

            return _spawns;
        }

        private string ReadFile(string path)
        {
            Interlocked.Increment(ref loadCounter);

            if (loadCounter % loadReportThreshold == 0)
            {
                int status = loadCounter / loadReportThreshold;
                _events.PublishOnUIThreadAsync(new LoadReport { Progress = status, LoadingExceptions = "" });
            }

            string jsonString = "";
            try
            {
                jsonString = File.ReadAllText(path);
            }
            catch (Exception e)
            {
                ExceptionStrings.Add($"{e.GetBaseException().Message} || Skipping File...{Path.GetFileName(path)}");
            }
            return jsonString;
        }

        private SpawnMapEntry ConvertFromJson(string jsonString, string path)
        {
            if (jsonString.Equals(""))
            {
                ExceptionStrings.Add($"{Path.GetFileName(path)} is an empty file. Skipping...");
                return null;
            }

            try
            {
                SpawnMapEntry map = JsonConvert.DeserializeObject<SpawnMapEntry>(jsonString);
                return map;
            }
            catch (Exception e)
            {
                ExceptionStrings.Add($"{e.GetBaseException().Message} || Skipping File...{Path.GetFileName(path)}");
                return null;
            }
        }

        private string[] GetFiles()
        {
            string[] allFiles = null;
            try
            {
                allFiles = Directory.GetFiles(_directoryPath, "*.json", SearchOption.AllDirectories);
            }
            catch (Exception e) //TODO: maybe expand this to catch all exceptions independently...
            {
                string newException = $"{e.InnerException.Message} | {e.InnerException}";
                ExceptionStrings.Add(newException);
            }

            return allFiles;
        }

    }
}
