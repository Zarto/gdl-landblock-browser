﻿using Lifestoned.DataModel.Gdle.Spawns;

namespace LBDataManagerLibrary.Processors
{

    public interface ISearchForEntity
    {
        int InstancesOfEntity { get; set; }
        int NumLandblocksWithEntity { get; set; }
        string Find(SpawnMap spawns, string weenieName = "", uint weenieID = 0, uint landblock = 0);
    }
}