﻿using Lifestoned.DataModel.Gdle.Spawns;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace LBDataManagerLibrary.Processors
{
    public class SearchForEntity : ISearchForEntity
    {
        private ConcurrentBag<string> foundLocation = new ConcurrentBag<string>();
        private ConcurrentBag<string> foundLandblockIDs = new ConcurrentBag<string>();
        public int InstancesOfEntity { get; set; } = 0;
        public int NumLandblocksWithEntity { get; set; } = 0;
        public string Find(SpawnMap spawns, string weenieName = "", uint weenieID = 0, uint landblock = 0)
        {
            if (weenieName.Equals("") && weenieID == 0)
            {
                return "No search parameters entered!";
            }

            if (!weenieName.Equals(""))
            {
                //TODO: search for ID from imported CSV and set weenieID to the mapped value.
            }

            foreach (SpawnMapEntry map in spawns.Entries)
            {
                if (landblock !=0 && landblock != map.Key)
                {
                    continue;
                }
                bool entityAlreadyFoundInLandblock = false;
                string mapName = map.Description;

                foreach (SpawnWeenie spawnWeenie in map.Value.Weenies)
                {
                    if (spawnWeenie.WeenieId == weenieID || (!weenieName.Equals("") &&spawnWeenie.Description.Contains(weenieName)))
                    {
                        //Get rid of location identifier...unnecessary
                        string Name = spawnWeenie.Description.Replace("  Dungeon", "").Replace("  Overworld", "");

                        //Get rid of duplicate landblockID that was a legacy import...
                        if (char.IsDigit(map.Description,0) && char.IsDigit(map.Description, 1))
                        {
                            mapName = map.Description.Substring(map.Description.IndexOf(" - ") + 3);
                        }

                        if (!entityAlreadyFoundInLandblock)
                        {
                            foundLandblockIDs.Add($"{map.Key} - {mapName}");
                            entityAlreadyFoundInLandblock = true;
                        }

                        foundLocation.Add($"{spawnWeenie.WeenieId} - {Name} | {map.Key} - {mapName} | {spawnWeenie.Position.Display}");
                    }

                }

            }

            return CompileReport();
        }

        private string CompileReport()
        {           
            if (foundLocation.IsEmpty)
            {
                return "No entries found!";
            }
            InstancesOfEntity = foundLocation.Count;
            NumLandblocksWithEntity = foundLandblockIDs.Count;
            string foundLocations = $"Number of Instances found: {InstancesOfEntity}{Environment.NewLine}" +
                $"Number of landblocks containing weenie instance: {NumLandblocksWithEntity}{Environment.NewLine}";

            foreach (string location in foundLandblockIDs)
            {
                foundLocations += location + Environment.NewLine;
            }

            foundLocations += $"=================================={Environment.NewLine}{Environment.NewLine}";

            foreach (string foundWeenie in foundLocation)
            {
                foundLocations += foundWeenie + Environment.NewLine;

            }
            return foundLocations;
        }
    }
}
