﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LBDataManagerLibrary.Models
{
    public class LoadReport
    {
        public int Progress { get; set; }
        public string LoadingExceptions { get; set; }
    }
}
