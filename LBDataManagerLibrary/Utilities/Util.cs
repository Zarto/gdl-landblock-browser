﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LBDataManagerLibrary.Utilities
{
    public static class Util
    {
        public static uint TryConvertToDec(string hexOrDecString)
        {
            //are we already a uint?
            if (uint.TryParse(hexOrDecString, out uint result))
            {
                return result;            
            }

            //try as a hex representation if we fail, throw exception.
            hexOrDecString = hexOrDecString.Substring(hexOrDecString.IndexOf('x') + 1);
            return UInt32.Parse(hexOrDecString, System.Globalization.NumberStyles.HexNumber);

            //if (UInt32.TryParse(hexOrDecString, out uint hexToDecResult))
            //{
            //    return hexToDecResult;
            //}
            //throw new InvalidOperationException("Number is not a valid uint or a valid Hex string");
        }
    }
}
